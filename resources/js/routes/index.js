import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "index",
        redirect: {name: "products"},
    },
    {
        path: "/products",
        name: "products",
        component: () => import("../pages/products/index"),
    },
    {
        path: "/categories",
        name: "categories",
        component: () => import("../pages/categories/index"),
    },
];

const router = new VueRouter({
    mode: "history",
    routes,
});

export default router;
