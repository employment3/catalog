import transport from "../plugins/axios";
import Vue from "vue";
import Vuex from "vuex";
import { resourceModule } from "@reststate/vuex";
import { state, actions, mutations, getters } from "./rootModule";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: state,
  actions: actions,
  mutations: mutations,
  getters: getters,
  modules: {
    products: resourceModule({ name: "products", httpClient: transport }),
    categories: resourceModule({ name: "categories", httpClient: transport }),
  },
});

export default store;
