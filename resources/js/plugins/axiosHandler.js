import Vue from "vue";
import transport from "./axios";
import router from "../routes";

const axiosHandlers = transport.interceptors.response.use(
  (response) => {
    if (response.status) {
      switch (response.status) {
        case 201:
          Vue.toasted.success("Сохранено", {
            duration: 3000,
          });
          break;
        case 202:
          Vue.toasted.success("Сохранено", {
            duration: 3000,
          });
          break;
        case 204:
          Vue.toasted.success("Удалено", {
            duration: 3000,
          });
      }
      console.log(response.status);
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    console.error(error);
    if (error.response.status) {
      switch (error.response.status) {
        case 400:
          Vue.toasted.error(
            error?.response?.data?.custom_message
              ? error?.response?.data?.custom_message
              : "Некорректный запрос",
            {
              duration: 3000,
            }
          );
          break;
        case 401:
          Vue.toasted.error("Пользователь не авторизован", {
            duration: 3000,
          });
          Vue.auth.logout();
          break;
        case 403:
          Vue.toasted.error("Действие запрещено", {
            duration: 3000,
          });
          break;
        case 404:
          Vue.toasted.error("Не найдено", {
            duration: 3000,
          });
          router.push({ name: "404" });
          break;
        case 405:
          Vue.toasted.error(
            error?.response?.data?.custom_message
              ? error?.response?.data?.custom_message
              : "Запрещено",
            {
              duration: 3000,
            }
          );
          break;
        case 422:
            if (error.response.data.custom_error) {
                Vue.toasted.error(error.response.data.custom_error, {
                    duration: 3000,
                });
            } else {
                Vue.toasted.error("Проверьте введенные данные", {
                    duration: 3000,
                });
            }

          break;
        case 429:
          Vue.toasted.error(
            "Пожалуйста, подождите. Вы делаете запросы слишком часто",
            {
              duration: 3000,
            }
          );
          break;
        case 500:
          Vue.toasted.error("Ошибка сервера", {
            duration: 3000,
          });
      }
      return Promise.reject(error.response);
    }
  }
);

export default axiosHandlers;
