<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\QueryBuilder\AllowedFilter;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
      'title',
      'price',
      'published'
    ];

    public static function getAllowedSorts(): array
    {
        return [
            'title',
            'price',
            'published'
        ];
    }

    public static function getAllowedFilters(): array
    {
        return [
            AllowedFilter::scope('search'),
            AllowedFilter::scope('trashed'),
            AllowedFilter::scope('price_from'),
            AllowedFilter::scope('price_to'),
            AllowedFilter::scope('category'),
            AllowedFilter::exact('published'),
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function scopeSearch(Builder $builder, $search) {
        return $builder->where('title', 'ILIKE', "%{$search}%");
    }

    public function scopePriceFrom(Builder $builder, $search): Builder
    {
        return $builder->where('price', '>=', $search);
    }

    public function scopePriceTo(Builder $builder, $search): Builder
    {
        return $builder->where('price', '<=', $search);
    }

    public function scopeCategory(Builder $builder, $search): Builder
    {
        return $builder->whereHas('categories', function (Builder $builder) use ($search) {
            return $builder->where('title', 'ILIKE', "%{$search}%");
        });
    }

    public function scopeTrashed(Builder $builder, $value): Builder
    {

        if ($value) {
            return $builder->withTrashed();
        } else return $builder;
    }
}
