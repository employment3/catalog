<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory(500)->create()->each(function ($product) {
            $ids = Category::inRandomOrder()->limit(5)->get()->pluck('id')->toArray();
            $product->categories()->sync($ids);
        });
    }
}
